#include <SoftwareSerial.h>

/* 
    Arduino           GM65 - UART
    2                   TX  (Đen)
    3                   RX  (Vàng)

*/


SoftwareSerial mySerial(2, 3); // RX, TX

void setup()
{
  Serial.begin(115200);
  mySerial.begin(115200);
  while (!Serial) {
    ; 
  }
  Serial.println("Hshop GM65 Demo!");
}

void loop() // run over and over
{
  if (mySerial.available()){
    String value = mySerial.readStringUntil('\n');
    Serial.println("Read BarCode: " + value);
  }
}
